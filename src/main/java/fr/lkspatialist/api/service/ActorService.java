package fr.lkspatialist.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import fr.lkspatialist.api.model.Actor;
import fr.lkspatialist.api.model.Movie;

import lombok.Data;
import fr.lkspatialist.api.repository.ActorRepository;

@Data
@Service
public class ActorService {
	
	@Autowired
	private ActorRepository actorRepository;
	
	public Optional<Actor> getActor(final Long id) {
		return actorRepository.findById(id);
	}
	
	public Iterable<Actor> getActors() {
		return actorRepository.findAll();
	}
	
	public void deleteActor(final Long id) {
		actorRepository.deleteById(id);
	}
	
	public Actor saveActor(Actor actor) {
		Actor savedActor = actorRepository.save(actor);
		return savedActor;
	}
        
        
	public Iterable<Movie> getAllMovies(final Long id) {
            if(getActor(id) != null){
                return getActor(id).get().getMovies();
            }else{
                return null;
            }
            /*Optional<Actor> actor = getActor(id);
            if(actor != null){
		return actor.get().getMovies();
            }else{
                return null;
            }*/
	}

}
