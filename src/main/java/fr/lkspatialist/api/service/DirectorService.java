package fr.lkspatialist.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import fr.lkspatialist.api.model.Actor;
import fr.lkspatialist.api.model.Director;

import lombok.Data;
import fr.lkspatialist.api.repository.DirectorRepository;

@Data
@Service
public class DirectorService {
	
	@Autowired
	private DirectorRepository directorRepository;
	
	public Optional<Director> getDirector(final Long id) {
		return directorRepository.findById(id);
	}
	
	public Iterable<Director> getDirectors() {
		return directorRepository.findAll();
	}
}
