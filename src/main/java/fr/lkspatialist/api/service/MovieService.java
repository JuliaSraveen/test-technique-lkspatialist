package fr.lkspatialist.api.service;

import fr.lkspatialist.api.model.Actor;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import fr.lkspatialist.api.model.Movie;
import fr.lkspatialist.api.model.Director;
import fr.lkspatialist.api.repository.ActorRepository;

import lombok.Data;
import fr.lkspatialist.api.repository.MovieRepository;
import fr.lkspatialist.api.repository.DirectorRepository;

@Data
@Service
public class MovieService {
	
	@Autowired
	private MovieRepository movieRepository;
        
	@Autowired
	private DirectorRepository directorRepository;
        
	@Autowired
	private ActorRepository actorRepository;
	
	public Optional<Movie> getMovie(final Long id) {
		return movieRepository.findById(id);
	}
	
	public Iterable<Movie> getMovies() {
		return movieRepository.findAll();
	}
        
	public Iterable<Movie> getMovieByDirector(final Long id) {
            Optional<Director> dir = directorRepository.findById(id);
            if(dir != null){
		return movieRepository.findByDirector(dir.get());
            }else{
                return null;
            }
	}
}
