package fr.lkspatialist.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sun.istack.NotNull;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "DIRECTOR")  
public class Director  implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="name")
	private String name;

        /*@OneToMany(mappedBy="dir")
        private Set<Movie> movies;*/
        
        /*@OneToMany(mappedBy = "director", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
        private Set<Movie> movies;*/
         @OneToMany(mappedBy = "director")
         @JsonIgnore
        private List<Movie> movies;
         
         
         
    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
         
	
}
