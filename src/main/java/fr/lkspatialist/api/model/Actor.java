package fr.lkspatialist.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "ACTOR")  
public class Actor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="name")
	private String name;
	
        
        /*@ManyToMany(cascade = CascadeType.ALL)
        public List<Movie> movies = new ArrayList<Movie>();*/
        
         
        @JsonIgnore
        @ManyToMany(mappedBy = "actors", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
        private List<Movie> movies = new ArrayList<Movie>();


    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
}
