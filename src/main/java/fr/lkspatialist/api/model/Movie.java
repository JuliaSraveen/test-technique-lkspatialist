package fr.lkspatialist.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table(name = "MOVIE")  
public class Movie  implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="title")
	private String title;
	
	@Column(name="date")
	private String date;
	
        /*@ManyToOne
        @JoinColumn(name="director_id")
	private Director dir;*/
        /*@ManyToOne(fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name = "director_id", nullable = false)
        private Director director;*/
         @ManyToOne
        @JoinColumn(name = "director_id", nullable = false)
        private Director director;

         public Director getDirector() {
            return director;
        }

        public void setDirector(Director director) {
            this.director = director;
    }
       
        @ManyToMany(
            cascade = CascadeType.MERGE,
            fetch = FetchType.EAGER
        )
        @JoinTable(
                name = "actor_movie",
                joinColumns = @JoinColumn(name = "movie_id", referencedColumnName = "id"),
                inverseJoinColumns = @JoinColumn(name = "actor_id", referencedColumnName = "id")
        )
        private List<Actor> actors = new ArrayList<Actor>();;
        /*@ManyToMany(mappedBy = "movies")
    public List<Actor> actors = new ArrayList<Actor>();*/
        
         
    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }
	
}
