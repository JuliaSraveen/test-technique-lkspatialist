package fr.lkspatialist.api.repository;

import fr.lkspatialist.api.model.Actor;
import fr.lkspatialist.api.model.Director;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Sort;

import fr.lkspatialist.api.model.Movie;
import java.util.List;

@Repository
public interface MovieRepository extends CrudRepository<Movie, Long>{

    List<Movie> findByDirector(Director director);
}