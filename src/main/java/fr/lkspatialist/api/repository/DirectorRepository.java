package fr.lkspatialist.api.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.lkspatialist.api.model.Director;

@Repository
public interface DirectorRepository extends CrudRepository<Director, Long>{

}