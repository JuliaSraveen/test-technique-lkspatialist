package fr.lkspatialist.api.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.lkspatialist.api.model.Actor;
import fr.lkspatialist.api.model.Movie;
import java.util.List;

@Repository
public interface ActorRepository extends CrudRepository<Actor, Long>{

    
    //List<Movie> findByActor(Actor actor);
}