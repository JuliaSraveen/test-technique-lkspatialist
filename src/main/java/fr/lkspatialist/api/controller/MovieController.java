package fr.lkspatialist.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.lkspatialist.api.model.Actor;
import fr.lkspatialist.api.model.Movie;
import fr.lkspatialist.api.service.MovieService;
import java.util.List;

@RestController
public class MovieController {
	
	@Autowired
	private MovieService movieService;
	
	/**
	 * Read - Get one movie 
	 * @param id The id of the movie
	 * @return An Movie object full filled
	 */
	@GetMapping("/movie/{id}")
	public Movie getMovie(@PathVariable("id") final Long id) {
		Optional<Movie> movie = movieService.getMovie(id);
		if(movie.isPresent()) {
			return movie.get();
		} else {
			return null;
		}
	}
	
	/**
	 * Read - Get all movies
	 * @return - An Iterable object of Movie full filled
	 */
	@GetMapping("/movies")
	public Iterable<Movie> getMovies() {
		return movieService.getMovies();
	}
        
        /**
	 * Read - Get all the movies of a director
	 * @param id - The id of the director
	 */
        @GetMapping("/movie/director/{id}")
	public Iterable<Movie> getMovieByDirector(@PathVariable("id") final Long id) {
            return movieService.getMovieByDirector(id);
	}
	
}