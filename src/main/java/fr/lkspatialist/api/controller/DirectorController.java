package fr.lkspatialist.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.lkspatialist.api.model.Actor;
import fr.lkspatialist.api.model.Director;
import fr.lkspatialist.api.service.DirectorService;

@RestController
public class DirectorController {
	
	@Autowired
	private DirectorService directorService;
	
	/**
	 * Read - Get one director 
	 * @param id The id of the director
	 * @return An Director object full filled
	 */
	@GetMapping("/director/{id}")
	public Director getDirector(@PathVariable("id") final Long id) {
		Optional<Director> director = directorService.getDirector(id);
		if(director.isPresent()) {
			return director.get();
		} else {
			return null;
		}
	}
	
	/**
	 * Read - Get all directors
	 * @return - An Iterable object of Director full filled
	 */
	@GetMapping("/directors")
	public Iterable<Director> getDirectors() {
		return directorService.getDirectors();
	}
	
}