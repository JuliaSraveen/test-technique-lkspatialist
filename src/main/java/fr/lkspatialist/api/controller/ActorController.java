package fr.lkspatialist.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.lkspatialist.api.model.Actor;
import fr.lkspatialist.api.model.Movie;
import fr.lkspatialist.api.service.ActorService;

@RestController
public class ActorController {
	
	@Autowired
	private ActorService actorService;
	
	/**
	 * Create - Add a new actor
	 * @param actor An object actor
	 * @return The actor object saved
	 */
	@PostMapping("/actor")
	public Actor createActor(@RequestBody Actor actor) {
		return actorService.saveActor(actor);
	}
	
	
	/**
	 * Read - Get one actor 
	 * @param id The id of the actor
	 * @return An Actor object full filled
	 */
	@GetMapping("/actor/{id}")
	public Actor getActor(@PathVariable("id") final Long id) {
		Optional<Actor> actor = actorService.getActor(id);
		if(actor.isPresent()) {
			return actor.get();
		} else {
			return null;
		}
	}
	
	/**
	 * Read - Get all actors
	 * @return - An Iterable object of Actor full filled
	 */
	@GetMapping("/actors")
	public Iterable<Actor> getActors() {
		return actorService.getActors();
	}
	
	/**
	 * Update - Update an existing actor
	 * @param id - The id of the actor to update
	 * @param actor - The actor object updated
	 * @return
	 */
	@PutMapping("/actor/{id}")
	public Actor updateActor(@PathVariable("id") final Long id, @RequestBody Actor actor) {
		Optional<Actor> e = actorService.getActor(id);
		if(e.isPresent()) {
			Actor currentActor = e.get();
			
			String name = actor.getName();
			if(name != null) {
				currentActor.setName(name);
			}
			actorService.saveActor(currentActor);
			return currentActor;
		} else {
			return null;
		}
	}
	
	
	/**
	 * Delete - Delete an actor
	 * @param id - The id of the actor to delete
	 */
	@DeleteMapping("/actor/{id}")
	public void deleteActor(@PathVariable("id") final Long id) {
		actorService.deleteActor(id);
	}
        
        /**
	 * Read - Get all the movies of an actor
	 * @param id - The id of the actor
	 */
        @GetMapping("/actor/{id}/movies")
	public Iterable<Movie> getMovieByActor(@PathVariable("id") final Long id) {
            Iterable<Movie> movies = (getActor(id) != null) ?  getActor(id).getMovies() :  null;
            return movies;
	}
	

}