package fr.lkspatialist.api;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class MovieControllerTest {

	@Autowired
	public MockMvc mockMvc;
	
	@Test
	public void testGetMovies() throws Exception {
		
		mockMvc.perform(get("/movie/director/2"))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$[1].title", is("Indiana Jones - Le temple maudit")));
                
		mockMvc.perform(get("/actor/1/movies"))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$[0].title", is("Star Wars 4 - Un nouvel espoir")));
		
	}
	
}
